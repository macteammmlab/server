<?php

namespace frontend\modules\v1\models;
use yii\db\Exception;
use yii\db\Query;
use backend\components\Earth;

use common\models\Food;
use common\models\Store;
use common\models\Comment;

class Analysis
{
	public static function getStoresNearBy($latitude, $longitude, $offset, $limit)
	{
		$condition = [
            'del_flag' => 0,
        ];
        
        $usng='';
        $grid_id = [];
        
        if (!empty($latitude) && !empty($longitude))
        {            
            $earth = new Earth;
            $usng= $earth->LLtoUSNG($latitude, $longitude, 3);
            
            $grid_id['like'] = substr($usng, 0, 6);
            $grid_id['row'] = substr($usng, 7, 2);
            $grid_id['col'] = substr($usng, 11, 2);
        }

        $location_condition = [
            'like',
            'grid_id',
            $grid_id['like'].'%', false
        ];
        
		$items = [];
        
        if (!empty($latitude) && !empty($longitude))
        {
			$items = Store::find()
				->where($condition)
				->andWhere($location_condition)
				->offset(null)
                ->limit(null)
				->asArray()
                ->all();
        }
        
        $returnValue = [];
			
		foreach($items as $item)
			$returnValue[] = $item;
		
		return $returnValue;
	}
	
	public static function getStoresByName($store_name)
	{
		$condition = [
            'del_flag' => 0,
        ];
		
		$store_condition = [
			'like',
			'name',
			'%'.$store_name.'%', false
		];
		
		$items = [];
		
		if(!empty($store_name))
		{
			$items = Store::find()
                ->where($condition)
                ->andWhere($store_condition)
                ->offset(null)
                ->limit(null)
                ->asArray()
                ->all();
		}
		
		$returnValue = [];
			
		foreach($items as $item)
		{
			$returnValue[] = $item;
		}	
		
		return $returnValue;
	}
	
	public static function getFoodByFoodId($food_id)
	{
		$food = Food::find()
			->where(['id' => $food_id])
			->one();
			
		return $food;
	}
	
	public static function getMenuByStoreId($store_id)
	{
		$items = Store::find()
			->select('menu')
			->where(['del_flag' => 0, 'id' => $store_id])
			->one();

		$foods_id = explode('|', $items['menu']);

		$menu = [];
		foreach($foods_id as $food_id)
		{
			$menu[] = Analysis::getFoodByFoodId($food_id);
		}
		
		return $menu;
	}
	
	public static function getCommentByCommentId($comment_id)
	{
		$comment = Comment::find()
			->where(['del_flag' => 0, 'id' => $comment_id])
			->one();
			
		return $comment;
	}
	
	public static function getCommentsByStoreId($store_id)
	{
		$condition = [
            'del_flag' => 0,
        ];

		$items = Store::find()
			->select('comments')
			->where($condition)
			->andWhere(['id' => $store_id])
			->offset(null)
			->one();
		
		$comments_id = explode('|', $items['comments']);

		$comments = [];
		foreach($comments_id as $comment_id)
		{
			$comments[] = Analysis::getCommentByCommentId($comment_id);
		}
		
		return $comments;
	}
}

?>