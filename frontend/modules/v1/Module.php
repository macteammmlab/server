<?php
namespace frontend\modules\v1;

/**
 * Created by PhpStorm.
 * User: hoangdieu
 * Date: 5/11/15
 * Time: 12:36 AM
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\v1\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
