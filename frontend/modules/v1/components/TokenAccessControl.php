<?php

namespace frontend\modules\v1\components;

use Yii;
use yii\base\ActionFilter;
use yii\base\Action;

use frontend\modules\v1\models\AccessToken;

/**
 *  Token base access controller.
 *  This access controller check wether an access_token 
 *  embedded in post request is valid or not. 
 */
class TokenAccessControl extends ActionFilter 
{
    public $denyCallback;
    
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ((!isset($this->only) && !in_array($action->id, $this->except)) 
            || in_array($action->id, $this->only))
        {
            // TODO check access_token is valid or not
            $request = Yii::$app->request;
            $access_token = $request->post('access_token');
            if (!empty($access_token)) 
            {
                if (AccessToken::checkValidAccessToken($access_token)) 
                {
                    return true;
                } 
            }
        } 
        
        if (isset($this->denyCallback)) 
        {
            call_user_func($this->denyCallback, $action);
        }

        return false;
    }
}