<?php

namespace frontend\modules\v1\controllers;
use frontend\modules\v1\models\AccessToken;
use Yii;
use yii\db\Exception;
use yii\web\Response;

use frontend\modules\v1\components\TokenAccessControl;
use DateTime;

use frontend\modules\v1\models\Analysis;

class ApiController extends \frontend\controllers\BaseController
{
	public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'  => [
                'class' => TokenAccessControl::className(),
                'except' => ['signin', 'singup', 'register', 'refreshtoken', 
							'getstoresnearby', 'getstoresbyname', 'getmenubystoreid', 'getcommentsbystoreid'],
                'denyCallback' => [$this, 'authenticationFailed'],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    '*' => ['post'],
                ],
            ],
        ];
    }

    public function authenticationFailed($action)
    {
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->statusCode = 403;
        $response->data = [
            'error' => 'token authentication failed',
        ];
    }
	
	public function actionGetstoresnearby()
	{
		$response = Yii::$app->response;
		$response->format = Response::FORMAT_JSON;

		if (Yii::$app->request->isPost)
        {
			$latitude = Yii::$app->request->post('latitude');
            $longitude = Yii::$app->request->post('longitude');

			if (empty($latitude) || empty($longitude))
            {
                $response->statusCode = 400;
            }
			else
			{
				$offset = Yii::$app->request->post('offset');
                if (empty($offset))
                    $offset = 0;
                $limit = Yii::$app->request->post('limit');
                if (empty($limit))
                    $limit = 20;

                $stores = Analysis::getStoresNearBy($latitude, $longitude, $offset, $limit);
                $response->data = [
                    'success'=> true,
                    'stores' => $stores
                ];
			}
		}
		else 
        {
            $response->statusCode = 403;
        }
        
        return $response;
	}
	
	public function actionGetstoresbyname()
	{
		$response = Yii::$app->response;
		$response->format = Response::FORMAT_JSON;
		
		if(Yii::$app->request->isPost)
		{
			$store_name = Yii::$app->request->post('store_name');
			
			if(empty($store_name))
				$response->statusCode = 400;
			else
			{
				$offset = Yii::$app->request->post('offset');
                if (empty($offset))
                    $offset = 0;
                $limit = Yii::$app->request->post('limit');
                if (empty($limit))
                    $limit = 20;
					
				$stores = Analysis::getStoresByName($store_name);
				
				$response->data = [
					'success' => true,
					'stores' => $stores
				];
			}
		}
		else 
        {
            $response->statusCode = 403;
        }
        
        return $response;
	}
    
    public function actionGetmenubystoreid()
    {
        $response = Yii::$app->response;
		$response->format = Response::FORMAT_JSON;
        
        if (Yii::$app->request->isPost)
        {
            $store_id = Yii::$app->request->post('store_id');

            if(empty($store_id))
                $response->statusCode = 400;
            else
            {
                $menu = Analysis::getMenuByStoreId($store_id);
                $response->data = [
                    'success'=> true,
                    'menu' => $menu
                ];
            }
        }
        else 
        {
            $response->statusCode = 403;
        }
        
        return $response;
    }
	
	public function actionGetcommentsbystoreid()
	{
		$response = Yii::$app->response;
		$response->format = Response::FORMAT_JSON;
		
		if (Yii::$app->request->isPost)
        {
			 $store_id = Yii::$app->request->post('store_id');

            if(empty($store_id))
                $response->statusCode = 400;
            else
            {
                $comments = Analysis::getCommentsByStoreId($store_id);
                $response->data = [
                    'success'=> true,
                    'comments' => $comments
                ];
            }
		}
		else 
        {
            $response->statusCode = 403;
        }
        
        return $response;
	}
}

?>