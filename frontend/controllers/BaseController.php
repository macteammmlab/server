<?php

namespace frontend\controllers;

class BaseController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}