<?php

namespace common\models;
use yii\db\Exception;
use Yii;


class BaseRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
            'class'  => \yii\behaviors\TimestampBehavior::className(),
            'attributes' => [
                \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ["created_time","updated_time"],
                \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ["updated_time"],
            ],
            'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class'  => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['del_flag'],
                ],
                'value' => function ($event) {
                    return 0;
                },
            ]
        ];
    }

    /**
     * Method to validate params that will be added to instance
     */
    public static function validateParameters($params)
    {
        $className = static::className();
        $instance = new $className;
        foreach($instance->attributes as $key => $value)
        {
            if (array_key_exists($key, $params))
            {
                $instance->$key = $params[$key];
            }
        }
        if ($instance->validate())
        {
            return [
                'success' => true,
                'instance' => $instance,
            ];
        } else
        {
            return [
                'success' => false,
                'errors' => $instance->getErrors(),
            ];
        }
    }

    /**
     *  Method to delete an instance. Actually, to turn del_flag to 1
     *  @params: $checkConditions => array of where condition.
     */
    public static function deleteInstance($checkConditions)
    {
        $results = [];
        $instance; 
        if (empty($checkConditions))
        {
            $results['success'] = false;
            $results['errors'] = ['Invalid parameters.'];
        } else
        {
            try {
                $instance = static::findOne($checkConditions);
                if (isset($instance))
                {
                    $instance->del_flag = 1;
                    if ($instance->save())
                    {
                        $results['success'] = true;
                        $results['instance'] = $instance;
                    } else
                    {
                        $results['success'] = false;
                        $results['errors'] = $instance->getErrors();
                    }
                } else
                {
                    $results['success'] = false;
                    $results['errors'] = ['Record does not exists.'];
                }
            } catch (Exception $e)
            {
                $results['success'] = false;
                $results['errors'] = $e->errorInfo;
            }
        }

        return $results;
    }

    /**
     * Method to create new instance of specific record.
     * If $checkConditions is provided, then search for 
     * the instance. If exists, then just returns.
     *
     */
    public static function createInstance($checkConditions, $params)
    {
        $results = [];
        $instance;
        if (!empty($checkConditions))
        {
            try {
                $instance = static::generateAndQuery($checkConditions)->one();
            } catch (Exception $e)
            {
                
            }
        }

        if (!isset($instance))
        {
            $className = static::className();
            $instance = new $className;
            foreach($instance->attributes as $key => $value)
            {
                if (array_key_exists($key, $params))
                {
                    $instance->$key = $params[$key];
                }
            }
            if ($instance->save())
            {
                $results['instance'] = $instance;
                $results['success'] = true;
            } else
            {
                $results['success'] = false;
                $results['errors'] = $instance->getErrors();
            }
        } else 
        {
            $results['instance'] = $instance;
            $results['success'] = true;
        }

        return $results;
    }

    public static function updateInstance($checkConditions, $params)
    {
        $results = [];
        $instance;
        if (empty($checkConditions))
        {
            $results['errors'] = ['Invalid checkConditions.'];
            $results['success'] = false;
        } else
        {
            try {
                $instance = static::generateAndQuery($checkConditions)->one();
                if (isset($instance))
                {
                    foreach($instance->attributes as $key => $value)
                    {
                        if (array_key_exists($key, $params))
                        {
                            $instance->$key = $params[$key];
                        }
                    }
                    if ($instance->save())
                    {
                        $results['success'] = true;
                        $results['instance'] = $instance;
                    } else 
                    {
                        $results['success'] = false;
                        $results['errors'] = $instance->getErrors();
                    }
                } else 
                {
                    $results['success'] = false;
                    $results['errors'] = ['Instance does not exist.'];
                }
            } catch (Exception $e)
            {
                $results['success'] = false;
                $results['errors'] = $e->errorInfo;
            }
        }

        return $results;
    }

    protected static function generateAndQuery($checkConditions)
    {
        $query = static::find();
        if (is_array($checkConditions))
        {
            foreach($checkConditions as $key => $value)
            {
                if (!is_string($key))
                {
                    if (is_array($value))
                    {
                        $query->andWhere($value);
                    }
                } else
                {
                    $query->andWhere([$key => $value]);
                }
            }
        }

        return $query;
    }
}