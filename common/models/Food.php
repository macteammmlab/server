<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "food".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name_vi
 * @property string $name_en
 * @property string $description_vi
 * @property string $description_en
 * @property string $image
 * @property string $unit
 * @property integer $unit_price
 * @property double $rating
 * @property string $fearture_comment
 * @property string $detail_comment
 * @property integer $status
 * @property string $created_time
 * @property string $updated_time
 * @property integer $del_flag
 */
class Food extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'food';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'unit_price', 'status', 'del_flag'], 'integer'],
            [['name_vi', 'name_en', 'description_vi', 'description_en', 'image', 'unit', 'fearture_comment', 'detail_comment'], 'string'],
            [['rating'], 'number'],
            [['created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name_vi' => 'Name Vi',
            'name_en' => 'Name En',
            'description_vi' => 'Description Vi',
            'description_en' => 'Description En',
            'image' => 'Image',
            'unit' => 'Unit',
            'unit_price' => 'Unit Price',
            'rating' => 'Rating',
            'fearture_comment' => 'Fearture Comment',
            'detail_comment' => 'Detail Comment',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'del_flag' => 'Del Flag',
        ];
    }
}
