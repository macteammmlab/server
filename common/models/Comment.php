<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $food_id
 * @property integer $store_id
 * @property integer $user_id
 * @property string $content
 * @property double $rating
 * @property integer $status
 * @property string $created_time
 * @property string $updated_time
 * @property integer $del_flag
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['food_id', 'store_id', 'user_id', 'status', 'del_flag'], 'integer'],
            [['content'], 'string'],
            [['rating'], 'number'],
            [['created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'food_id' => 'Food ID',
            'store_id' => 'Store ID',
            'user_id' => 'User ID',
            'content' => 'Content',
            'rating' => 'Rating',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'del_flag' => 'Del Flag',
        ];
    }
}
