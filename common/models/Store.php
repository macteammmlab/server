<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "store".
 *
 * @property integer $id
 * @property string $name
 * @property string $location
 * @property string $grid_id
 * @property string $description_vi
 * @property string $description_en
 * @property string $open_time
 * @property string $close_time
 * @property string $image
 * @property string $menu
 * @property double $rating
 * @property string $comment
 * @property integer $status
 * @property string $created_time
 * @property string $updated_time
 * @property integer $del_flag
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'location', 'grid_id', 'description_vi', 'description_en', 'open_time', 'close_time', 'image', 'menu', 'comment'], 'string'],
            [['rating'], 'number'],
            [['status', 'del_flag'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'location' => 'Location',
            'grid_id' => 'Grid ID',
            'description_vi' => 'Description Vi',
            'description_en' => 'Description En',
            'open_time' => 'Open Time',
            'close_time' => 'Close Time',
            'image' => 'Image',
            'menu' => 'Menu',
            'rating' => 'Rating',
            'comment' => 'Comment',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'del_flag' => 'Del Flag',
        ];
    }
}
